import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Input() formHeaderText:string;
  @Input() formLogin:string;
  @Input() messageLogin: string;

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

  onLoginClick() {
    this.loginAttempt.emit('Pleace insert valid information');
  }


}

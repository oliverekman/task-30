import { Component, Input, Output,  EventEmitter } from '@angular/core';
import { Xtb } from '@angular/compiler';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {

  @Input() messageRegister: string;
  @Input() formRegister:string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter;

  onRegisterClick() {
    this.registerAttempt.emit('You have to create an account')
  }



}
